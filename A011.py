# Grupo 11  Daniel Serafim N 89428  Daniel Matos N 89429

import random

# LearningAgent to implement
# no knowledeg about the environment can be used
# the code should work even with another environment
class LearningAgent:

	# nS maximum number of states
	# nA maximum number of action per state
	def __init__(self, nS, nA):
		self.nS = nS
		self.nA = nA
		self.alpha = random.uniform(0.5, 0.8)
		self.gamma = random.uniform(0.7, 1)
		self.epsilon = 0.5
		self.Q = [[0 for i in range(nA)] for j in range(nS)]
		self.F = [[0 for i in range(nA)] for j in range(nS)]

	# Select one action, used when learning
	# st - is the current state
	# aa - is the set of possible actions
	# for a given state they are always given in the same order
	# returns
	# a - the index to the action in aa
	def selectactiontolearn(self, st, aa):
		# print("select one action to learn better")

		if max(self.F[st]) == 0:
			self.Q[st] = [0 for i in range(len(aa))]
			self.F[st] = [0 for i in range(len(aa))]

		if random.uniform(0,1) < self.epsilon:
			a = self.selectMaxQ(st, aa)
		else:
			a = self.selectMinFreq(st, aa)

		self.F[st][a] += 1

		return a

	# Select one action, used when evaluating
	# st - is the current state
	# aa - is the set of possible actions
	# for a given state they are always given in the same order
	# returns
	# a - the index to the action in aa
	def selectactiontoexecute(self, st, aa):
		a = self.selectMaxQ(st, aa)

		# print("select one action to see if I learned")
		return a

	# this function is called after every action
	# ost - original state
	# nst - next state
	# a - the index to the action taken
	# r - reward obtained
	def learn(self, ost, nst, a, r):
		#print("learn something from this data")
		prev = self.Q[ost][a]
		self.Q[ost][a] = prev + self.alpha*(r + self.gamma*max(self.Q[nst]) - prev)

		return

	def selectMinFreq(self, state, actions):
		return self.F[state].index(min(self.F[state]))

	def selectMaxQ(self, state, actions):
		return self.Q[state].index(max(self.Q[state]))
